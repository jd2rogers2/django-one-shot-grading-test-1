from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"todolists": lists}
    return render(request, "todos/lists.html", context)


def show_todolist(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_object": todo,
    }
    return render(request, "todos/detail.html", context)


def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def edit_todolist(request, id):
    todolist_object = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist_object)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist_object)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_todolist(request, id):
    todolist_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def add_todotask(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/addtask.html", context)


def update_todotask(request, id):
    todotask_object = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todotask_object)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.list.id)
    else:
        form = TodoItemForm(instance=todotask_object)
    context = {
        "form": form,
    }
    return render(request, "todos/edittask.html", context)
