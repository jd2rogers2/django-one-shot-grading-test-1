from django.urls import path
from todos.views import (
    todo_list_list,
    show_todolist,
    add_todotask,
    update_todotask,
)
from todos.views import create_todolist, edit_todolist, delete_todolist


urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_todolist, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", edit_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", add_todotask, name="todo_item_create"),
    path("items/<int:id>/edit/", update_todotask, name="todo_item_update"),
]
